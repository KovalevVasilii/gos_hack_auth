FROM python:3

ENV PYTHONUNBUFFERED 1
ENV DOCKERED 1
RUN mkdir /app
WORKDIR /app
COPY requirements.txt /app/
RUN pip install -r requirements.txt
COPY . /app/
WORKDIR /app/

EXPOSE 5000

COPY ./start.sh /app/
RUN chmod +x /app/start.sh
CMD ["bash", "/app/start.sh"]