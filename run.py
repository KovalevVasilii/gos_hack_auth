from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
import click
app = Flask('gos_hack')
api = Api(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'MeGa_S3cR3t_K39!'
app.config['PROPAGATE_EXCEPTIONS'] = True
db = SQLAlchemy(app)


@app.before_first_request
def create_tables():
    db.create_all()


app.config['JWT_SECRET_KEY'] = 'MeGa_S3cR3t_K39!'
jwt = JWTManager(app)

import views, models, resources

api.add_resource(resources.UserRegistration, '/registration')
api.add_resource(resources.UserLogin, '/login')
api.add_resource(resources.TokenRefresh, '/token/refresh')


@app.cli.command("run_server")
def run_server():
    print("-------------TEST--------------------")
    app.run()


# if __name__ == '__main__':
    # run_server()
    #db.create_all()
    # app.run(debug=True, port=5001)