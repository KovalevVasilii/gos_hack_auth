from flask_restful import Resource, reqparse
from models import UserModel
import re
import json
import requests
from run import jwt

from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    jwt_refresh_token_required,
    get_jwt_identity,
    get_raw_jwt,
    get_jwt_claims,
    JWTManager
    )

parser_registration = reqparse.RequestParser()
parser_registration.add_argument(
    'mail', help='This field cannot be blank', required=True)
parser_registration.add_argument(
    'phone', help='This field cannot be blank', required=True)
parser_registration.add_argument(
    'snils', help='This field cannot be blank', required=True)
parser_registration.add_argument(
    'password', help='This field cannot be blank', required=True)

parser = reqparse.RequestParser()
parser.add_argument(
    'login', help='This field cannot be blank')
parser.add_argument(
    'password', help='This field cannot be blank')


class UserRegistration(Resource):
    def post(self):
        data = parser_registration.parse_args()
        if UserModel.find_by_mail(data['mail']):
            return {
                'message': 'User with {} already exists'.format(data['mail'])}
        if UserModel.find_by_phone(data['phone']):
            return {
                'message': 'User with {} already exists'.format(data['phone'])}
        if UserModel.find_by_snils(data['snils']):
            return {
                'message': 'User with {} already exists'.format(data['snils'])}

        new_user = UserModel(
            mail=data['mail'],
            phone=data['phone'],
            snils=data['snils'],
            password=UserModel.generate_hash(data['password'])
        )


        try:
            new_user.save_to_db()

            headers = {'content-type': 'application/json; charset=utf-8'}

            response = {
                'Id': new_user.id,
                'Email': str(new_user.mail),
                'Snils': str(new_user.snils),
                'Phone': str(new_user.phone)
            }
            s = requests.Session()
            s.headers.update({'content-type': 'application/json; charset=utf-8'})
            print(s.headers)

            a = s.post('https://digitalhack20190721123423.azurewebsites.net/api/Account/Register', data=json.dumps(response))
            print(a.text)

            # access_token = create_access_token(identity=data['username'])
            # refresh_token = create_refresh_token(identity=data['username'])
            return {
                'message': f"User {data['mail']} was created",
                # 'access_token': access_token,
                # 'refresh_token': refresh_token
                }
        except Exception as e:
            return {'message': 'Something went wrong - {}'.format(e)}, 500


def check_if_phone(login):
    if '+' not in login:
        return None
    login = str(login).replace(' ', '')
    login = str(login).replace('-', '')
    login = str(login).replace('.', '')
    login = str(login).replace('+', '')
    login = str(login).replace('(', '')
    login = str(login).replace(')', '')
    if len(login) == 11:
        login = login[1::]
        login = '8' + login
    elif len(login) != 10:
        return None
    return login


def check_if_mail(login):
    match = re.search(r'[\w.-]+@[\w.-]+\.?[\w]+?', str(login))
    if match:
        return match.string
    return None


def check_if_snils(login):
    login = str(login).replace(' ', '')
    login = str(login).replace('-', '')
    login = str(login).replace('.', '')
    login = str(login).replace('+', '')
    login = str(login).replace('(', '')
    login = str(login).replace(')', '')
    if len(login) == 11:
        return login
    return None


def define_login(login):
    res = check_if_phone(login)
    if res:
        return 'phone', res
    res = check_if_mail(login)
    if res:
        return 'mail', res
    res = check_if_snils(login)
    if res:
        return 'snils', res


class UserLogin(Resource):
    def post(self):
        data = parser.parse_args()
        login = define_login(data['login'])
        data['login'] = login[1]
        current_user = None

        if login[0] == 'phone':
            current_user = UserModel.find_by_phone(login[1])
        elif login[0] == 'snils':
            current_user = UserModel.find_by_snils(login[1])
        elif login[0] == 'mail':
            current_user = UserModel.find_by_mail(login[1])

        if not current_user:
            return {
                'message': 'User doesn\'t exist'}

        if UserModel.verify_hash(data['password'], current_user.password):
            access_token = create_access_token(identity=current_user.id, expires_delta=False)
            refresh_token = create_refresh_token(identity=current_user.id, expires_delta=False)

            return {
                'message': 'Logged in as {}'.format(str(data['login'])),
                'access_token': access_token,
                'refresh_token': refresh_token
                }
        else:
            return {'message': 'Wrong credentials'}


# class UserLogoutAccess(Resource):
#     @jwt_required
#     def post(self):
#         jti = get_raw_jwt()['jti']
#         try:
#             revoked_token = RevokedTokenModel(jti=jti)
#             revoked_token.add()
#             return {'message': 'Access token has been revoked'}
#         except Exception as e:
#             return {'message': 'Something went wrong - {}'.format(e)}, 500


# class UserLogoutRefresh(Resource):
#     @jwt_refresh_token_required
#     def post(self):
#         jti = get_raw_jwt()['jti']
#         try:
#             revoked_token = RevokedTokenModel(jti=jti)
#             revoked_token.add()
#             return {'message': 'Refresh token has been revoked'}
#         except Exception as e:
#             return {'message': 'Something went wrong - {}'.format(e)}, 500


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        refresh_token = create_refresh_token(identity=current_user)
        return {'access_token': access_token, 'refresh_token': refresh_token}


class AllUsers(Resource):
    @jwt_required
    def get(self):
        return UserModel.return_all()

    @jwt_required
    def delete(self):
        return UserModel.delete_all()


# class SecretResource(Resource):
#     def get(self):
#         return {
#             'answer': 42
#         }
