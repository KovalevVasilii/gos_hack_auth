from run import db
from passlib.hash import pbkdf2_sha256 as sha256


class UserModel(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    mail = db.Column(db.String(120), unique=True, nullable=False)
    phone = db.Column(db.String(13), unique=True, nullable=False)
    snils = db.Column(db.String(13), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_mail(cls, mail):
        return cls.query.filter_by(mail=mail).first()

    @classmethod
    def find_by_phone(cls, phone):
        return cls.query.filter_by(phone=phone).first()

    @classmethod
    def find_by_snils(cls, snils):
        return cls.query.filter_by(snils=snils).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'mail': x.mail,
                'phone': x.phone,
                'snils': x.snils,
                'password': x.password
            }
        return {'users': list(map(
            lambda x: to_json(x), UserModel.query.all()))}

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except Exception as e:
            return {'message': 'Something went wrong - {}'.format(e)}

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)

